<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function () {
        return view('dashboard');
    });

    Route::get('rekap/pendapatan', 'RekapController@jumlahPendapatanTiapBulan')->name('rekap.pendapatan');

    Route::get('/pelanggan/create', 'PelangganController@create')->name('pelanggan.create');
    Route::get('/pelanggan/index', 'PelangganController@index')->name('pelanggan.index');
    Route::post('/pelanggan/create', 'PelangganController@store')->name('pelanggan.store');
    Route::delete('/pelanggan/{pelanggan}/delete', 'PelangganController@destroy')->name('pelanggan.destroy');
    Route::delete('/pelanggan/index', 'PelangganController@destroyAll')->name('pelanggan.destroyAll');
    Route::get('/pelanggan/{pelanggan}/edit', 'PelangganController@edit')->name('pelanggan.edit');
    Route::patch('/pelanggan/{pelanggan}/edit', 'PelangganController@update')->name('pelanggan.update');
    Route::get('/pelanggan/pencarian', 'PelangganController@pencarian')->name('pelanggan.pencarian');



    Route::get('/pembagian/index', 'PembagiantugasController@index')->name('pembagian.index');
    Route::post('/pembagian/index/store', 'PembagiantugasController@store')->name('pembagian.store');

    Route::get('/pembagian/index/{pelanggan}/batal', 'PembagiantugasController@batal')->name('pembagian.batal');

    Route::get('/rekap/index', 'RekapController@index')->name('rekap.index');
    Route::get('/rekap/index2', 'RekapController@index2')->name('rekap.index2');
    Route::get('/rekap/details/{kodeSewa}', 'RekapController@viewDetails')->name('rekap.details');
    Route::get('/rekap/detailsDas/{bulan_id}', 'RekapController@viewDetailsDas')->name('rekap.detailsDas');

    Route::get('/pembayaran/index', 'PembayaranController@index')->name('pembayaran.index');
    Route::post('/pembayaran/store', 'PembayaranController@store')->name('pembayaran.store');
    Route::delete('/rekap/{kodeBayar}/delete', 'PembayaranController@destroy')->name('details.destroy');

    Route::get('/staf/profile', 'HomeController@profile')->name('staf.profile');
    Route::get('/staf/index', 'HomeController@index')->name('staf.index');
    Route::get('/staf/create', 'HomeController@create')->name('staf.create');
    Route::post('/staf/create', 'HomeController@store')->name('staf.store');
    Route::get('/staf/{user}/edit', 'HomeController@edit')->name('staf.edit');
    Route::patch('/staf/{user}/edit', 'HomeController@update')->name('staf.update');
    Route::delete('/staf/{user}/delete', 'HomeController@destroy')->name('staf.delete');


});
Auth::routes();
Route::get('/auth/login', 'HomeController@index')->name('home');

