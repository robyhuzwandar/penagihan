@extends('admin')
@section('content')
    <!-- Main content -->
    <div class="box box-warning">
        <div class="box-header">
            <h3 class="box-title">Pembayaran</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col col-md-4">
                <form action="{{ route('pembayaran.store') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group {{ $errors->has('pelanggan') ? ' has-error' : '' }}">
                        <label>Pilih Pelanggan</label><br>
                        <select id="basic2" class="show-tick form-control" name="pelanggan" multiple>
                            <option disabled="" selected="">Pilih Pelanggan</option>
                            @foreach($pelanggan as $p)
                                <option value="{{ $p->pelanggan_id }}">{{ $p->namaPelanggan }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('pelanggan'))
                            <span class="help-block"><strong>{{ $errors->first('pelanggan') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('bulan') ? ' has-error' : '' }}">
                        <label>Pilih Bulan</label><br>
                        <select id="basic3" class="show-tick form-control" name="bulan" multiple>
                            <option disabled="" selected="">Pilih Bulan</option>
                            @foreach($bulan as $b)
                                <option value="{{ $b->id }}">{{ $b->nama }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('bulan'))
                            <span class="help-block"><strong>{{ $errors->first('bulan') }}</strong></span>
                        @endif
                    </div>
                    @include('template.alert')
                    <button type="submit" class="btn btn-primary btn-flat" name="submit">
                        <i class="fa fa-tag"> Simpan</i>
                    </button>
                </form>
            </div>

        </div>
        <!-- /.box-body -->
    </div>
@endsection