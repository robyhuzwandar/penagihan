@extends('admin')
@section('content')
    <!-- Main content -->
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title">Data Pelanggan</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>No Hp</th>
                    <th>Mulai Berlangganan</th>
                    <th>Bandwith</th>
                    <th>Jumlah Bayar</th>
                    @if(Auth::user()->role == 0)
                        <th>Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>

                @foreach($pelanggan as $p)
                    <tr>
                        <td width="3%">{{ $loop->index + 1 }}</td>
                        <td id="#" width="16%">{{ $p->nama }}</td>
                        <td width="16%">{{ $p->alamat }}</td>
                        <td width="16%">{{ $p->nohp }}</td>
                        <td width="16%">{{ $p->namaBulan }}</td>
                        <td width="16%">{{ $p->bandwith }}</td>
                        <td width="16%">{{ number_format($p->jumlahBayar) }}</td>
                        <td width="16%" class="text-center">
                            @if(Auth::user()->role == 0)
                                <div class="row">
                                    <div class="col col-sm-2">
                                        <a href="{{ route('pelanggan.edit', $p->id) }}"
                                           data-toggle="tooltip" data-placement="top" title="Edit"
                                           class="btn btn-xs btn-warning btn-flat"><i
                                                    class="glyphicon glyphicon-edit"></i></a>
                                    </div>
                                    <div class="col col-sm-2">
                                        <form class="" action="{{ route('pelanggan.destroy', $p->id)}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button onclick="window.alert('Hapus pelanggan ?')" type="submit"
                                                    data-toggle="tooltip" data-placement="top" title="Hapus"
                                                    name="button" class="btn btn-xs btn-danger btn-flat"><span
                                                        class="glyphicon glyphicon-trash"></span></button>
                                        </form>
                                    </div>
                                </div>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection