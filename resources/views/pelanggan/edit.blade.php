@extends('admin')
@section('content')
    <!-- Main content -->
    <div class="box box-warning">
        <div class="box-header">
            <h3 class="box-title">Edit Pelanggan</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    @include('template.alert')
                    <form action="{{ route('pelanggan.update', $pelanggan)}}" method="post"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <div class="form-group {{ $errors->has('nama') ? ' has-error' : '' }}">
                            <label>Nama</label>
                            <input value="{{ $pelanggan->nama }}" type="text" class="form-control" name="nama">
                            @if ($errors->has('nama'))
                                <span class="help-block"><strong>{{ $errors->first('nim') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('alamat') ? ' has-error' : '' }}">
                            <label>Alamat</label>
                            <input value="{{ $pelanggan->alamat }}" type="text" class="form-control" name="alamat">
                            @if ($errors->has('alamat'))
                                <span class="help-block"><strong>{{ $errors->first('alamat') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('nohp') ? ' has-error' : '' }}">
                            <label>No HP</label>
                            <input value="{{ $pelanggan->nohp }}" type="text" class="form-control" name="nohp">
                            @if ($errors->has('nohp'))
                                <span class="help-block"><strong>{{ $errors->first('nohp') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group  {{ $errors->has('bandwith') ? ' has-error' : '' }}">
                            <label>Bandwith</label>
                            <input value="{{ $pelanggan->bandwith }}" type="text" class="form-control" name="bandwith">
                            @if ($errors->has('bandwith'))
                                <span class="help-block"><strong>{{ $errors->first('bandwith') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group  {{ $errors->has('jumlahBayar') ? ' has-error' : '' }}">
                            <label>Jumlah Bayar</label>
                            <input value="{{ $pelanggan->jumlahBayar}}" type="text" class="form-control"
                                   name="jumlahBayar">
                            @if ($errors->has('jumlahBayar'))
                                <span class="help-block"><strong>{{ $errors->first('jumlahBayar') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Pilih Bulan Mulai</label><br>
                            <select id="basic3" class="show-tick form-control" name="bulan" multiple>
                                <option disabled="">Pilih Bulan</option>
                                @foreach($bulan as $b)
                                    <option
                                            @if($b->id == $pelanggan->bulan_id)
                                            selected
                                            @endif
                                            value="{{ $b->id }}">{{ $b->nama }}</option>
                                @endforeach
                            </select>
                        </div>

                        <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection