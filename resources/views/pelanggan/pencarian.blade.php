@extends('admin')
@section('content')
    <!-- Main content -->
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title">Pencarian</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Pelanggan</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding table-responsive">
                    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>No Hp</th>
                            <th>Mulai Berlangganan</th>
                            <th>Bandwith</th>
                            <th>Jumlah Bayar</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($pelanggan as $p)
                            <tr>
                                <td width="3%">{{ $loop->index + 1 }}</td>
                                <td id="#" width="16%">{{ $p->nama }}</td>
                                <td width="16%">{{ $p->alamat }}</td>
                                <td width="16%">{{ $p->nohp }}</td>
                                <td width="16%">{{ $p->namaBulan }}</td>
                                <td width="16%">{{ $p->bandwith }}</td>
                                <td width="8%">{{ number_format($p->jumlahBayar) }}</td>
                                <td width="16%" class="text-center">
                                    @if(Auth::user()->role == 0)
                                        <div class="row">
                                            <div class="col col-sm-2">
                                                <a href="{{ route('pelanggan.edit', $p->id) }}"
                                                   data-toggle="tooltip" data-placement="top" title="Edit"
                                                   class="btn btn-xs btn-warning btn-flat"><i
                                                            class="glyphicon glyphicon-edit"></i></a>
                                            </div>
                                            <div class="col col-sm-2">
                                                <form class="" action="{{ route('pelanggan.destroy', $p->id)}}" method="post">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <button onclick="window.alert('Hapus pelanggan ?')" type="submit"
                                                            data-toggle="tooltip" data-placement="top" title="Hapus"
                                                            name="button" class="btn btn-xs btn-danger btn-flat"><span
                                                                class="glyphicon glyphicon-trash"></span></button>
                                                </form>
                                            </div>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>

            {{--kedua--}}

            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Details Pembayaran</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding table-responsive">
                    <table id="example1" class="table table-striped table-bordered" border="1" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Pelanggan</th>
                            <th>Alamat</th>
                            <th>No Hp</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pelangganBayar as $p)
                            <tr>
                                <td width="3%">{{ $loop->index + 1 }}</td>
                                <td width="16%">{{ $p->nama }}</td>
                                <td width="16%">{{ $p->alamat }}</td>
                                <td width="16%">{{ $p->nohp }}</td>
                                <td width="16%">
                                    <button type="button" class="btn btn-info btn-xs"
                                            data-toggle="modal" data-target-id="{{ $p->kodeSewa }}"
                                            data-target="#myModal">
                                        <i class="fa fa-eye"></i> Details
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <!-- Modal -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Details Pembayaran</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="fect-modal-body"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>


            {{--ketiga--}}
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Rekapan Pembayaran</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding table-responsive">
                    <table id="example1" class="table table-striped table-bordered" border="1" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th rowspan="2" class="text-center">No</th>
                            <th rowspan="2" class="text-center">Nama</th>
                            <th rowspan="2" class="text-center">Bulan Mulai</th>
                            <th colspan="12" class="text-center">Bulan</th>
                        </tr>
                        <tr>
                            @foreach($bulanAll as $bln)
                                <th>{{ $bln->nama }}</th>
                            @endforeach
                            {{--<th>Action</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pelangganLapor as $p)
                            @if($p->pelangganNama != NULL)
                                <tr>
                                    <td width="3%">{{ $loop->index+1}}</td>
                                    <td width="16%"> {{ $p->pelangganNama }}</td>
                                    <td width="16%">
                                        <small data-toggle="tooltip" data-placement="top"
                                               title="{{ $p->stafNama }}" class="label label-success">
                                            @if($p->bulanMulai == 1)
                                                Januari
                                            @elseif($p->bulanMulai == 2)
                                                Februari
                                            @elseif($p->bulanMulai == 3)
                                                Maret
                                            @elseif($p->bulanMulai == 4)
                                                April
                                            @elseif($p->bulanMulai == 5)
                                                Mei
                                            @elseif($p->bulanMulai == 6)
                                                Juni
                                            @elseif($p->bulanMulai == 7)
                                                Juli
                                            @elseif($p->bulanMulai == 8)
                                                Agustus
                                            @elseif($p->bulanMulai == 9)
                                                September
                                            @elseif($p->bulanMulai == 10)
                                                Oktober
                                            @elseif($p->bulanMulai == 11)
                                                November
                                            @elseif($p->bulanMulai == 12)
                                                Desember
                                            @endif
                                        </small>


                                    </td>
                                    <td width="16%">{{ number_format( $p->Januari) }}</td>
                                    <td width="16%">{{ number_format($p->Februari) }}</td>
                                    <td width="16%">{{ number_format($p->Maret) }}</td>
                                    <td width="16%">{{ number_format($p->April) }}</td>
                                    <td width="16%">{{ number_format($p->Mei) }}</td>
                                    <td width="16%">{{ number_format($p->Juni) }}</td>
                                    <td width="16%">{{ number_format($p->Juli) }}</td>
                                    <td width="16%">{{ number_format($p->Agustus) }}</td>
                                    <td width="16%">{{ number_format($p->September) }}</td>
                                    <td width="16%">{{ number_format($p->Oktober) }}</td>
                                    <td width="16%">{{ number_format($p->November) }}</td>
                                    <td width="16%">{{ number_format($p->Desember) }}</td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <!-- /.box-body -->
    </div>
@endsection