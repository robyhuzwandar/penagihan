@extends('admin')
@section('content')
    <!-- Main content -->
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title">Pendaftaran Staf</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                @if(Auth::user()->role == 0)
                    <div class="col col-md-5">
                        @include('template.alert')
                        <form method="POST" action="{{ route('staf.store') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group has-feedback{{ $errors->has('nama') ? ' has-error' : '' }}">
                                <input placeholder="Fullname" id="nama" type="text" class="form-control" name="nama"
                                       value="{{ old('nama') }}" required autofocus>
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                @if ($errors->has('name'))
                                    <span class="help-block"><strong>{{ $errors->first('nama') }}</strong></span>
                                @endif
                            </div>

                            <div class="form-group has-feedback{{ $errors->has('alamat') ? ' has-error' : '' }}">
                                <input placeholder="Alamat" id="alamat" type="text" class="form-control" name="alamat"
                                       value="{{ old('alamat') }}" required autofocus>
                                <span class="fa fa-home form-control-feedback"></span>
                                @if ($errors->has('alamat'))
                                    <span class="help-block"><strong>{{ $errors->first('alamat') }}</strong></span>
                                @endif
                            </div>

                            <div class="form-group has-feedback{{ $errors->has('nohp') ? ' has-error' : '' }}">
                                <input placeholder="No hp" id="nohp" type="number" class="form-control" name="nohp"
                                       value="{{ old('nohp') }}" required autofocus>
                                <span class="fa  fa-phone form-control-feedback"></span>
                                @if ($errors->has('nohp'))
                                    <span class="help-block"><strong>{{ $errors->first('nohp') }}</strong></span>
                                @endif
                            </div>

                            <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input placeholder="Email" id="email" type="email" class="form-control" name="email"
                                       value="{{ old('email') }}" required autofocus>
                                <span class="fa  fa-envelope form-control-feedback"></span>
                                @if ($errors->has('email'))
                                    <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                @endif
                            </div>

                            <div class="form-group has-feedback{{ $errors->has('foto') ? ' has-error' : '' }}">
                                <input id="foto" type="file" class="form-control" name="foto" value="{{ old('foto') }}"
                                       required
                                       autofocus>
                                @if ($errors->has('foto'))
                                    <span class="help-block"><strong>{{ $errors->first('foto') }}</strong></span>
                                @endif
                            </div>

                            <div class="form-group has-feedback{{ $errors->has('username') ? ' has-error' : '' }}">
                                <input placeholder="Username" id="username" type="text" class="form-control"
                                       name="username"
                                       value="{{ old('username') }}" required>
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                @if ($errors->has('username'))
                                    <span class="help-block"><strong>{{ $errors->first('username') }}</strong></span>
                                @endif
                            </div>

                            <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input id="password" placeholder="Password" type="password" class="form-control"
                                       name="password"
                                       required>
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                @if ($errors->has('password'))
                                    <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                                @endif
                            </div>

                            <div class="form-group has-feedback">
                                <input placeholder="Retype password" id="password-confirm" type="password"
                                       class="form-control"
                                       name="password_confirmation" required>
                                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                            </div>
                            <button type="submit" class="btn btn-primary btn-flat">
                                <i class="fa fa-user-plus"> Register</i>
                            </button>

                        </form>
                    </div>
                @else
                    <h1 class="text-center">Anda Tidak Memiliki Akses</h1>
                @endif
            </div>
        </div>
        <!-- /.box-body -->
    </div>
@endsection