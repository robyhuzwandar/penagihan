@extends('admin')
@section('content')
    <!-- Main content -->
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title">Data Pegawai</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>No Hp</th>
                    @if(Auth::user()->role == 0)
                        <th>Username</th>
                    @endif
                    <th>Email</th>
                    <th>Role</th>
                    <th>Foto</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($user as $u)
                    <tr>
                        <td width="3%">{{ $loop->index + 1 }}</td>
                        <td id="#" width="16%">{{ $u->nama }}</td>
                        <td width="16%">{{ $u->alamat }}</td>
                        <td width="16%">{{ $u->nohp }}</td>
                        @if(Auth::user()->role == 0)
                            <td width="16%">{{ $u->username }}</td>
                        @endif
                        <td width="16%">{{ $u->email }}</td>
                        <td width="16%">
                            @if($u->role == 0)
                                Kepala Staf
                            @else
                                Staf
                            @endif
                        </td>
                        <td width="16%">
                            <img src="{{ asset('fotoStaf/'. $u->foto) }}" style=" height:45px; width:auto important;"
                                 class="img-circle" alt="User Image">
                        </td>
                        <td width="16%" class="text-center">
                            <div class="row">
                                @if(Auth::user()->id == $u->id && Auth::user()->role == 1)
                                    <div class="col col-sm-2">
                                        <a href="{{ route('staf.edit', $u->id) }}"
                                           data-toggle="tooltip" data-placement="top" title="Edit"
                                           class="btn btn-warning btn-xs btn-flat"><i class="fa  fa-edit"></i></a>
                                    </div>
                                @elseif(Auth::user()->role == 0)
                                    <div class="col col-sm-2">
                                        <a href="{{ route('staf.edit', $u->id) }}"
                                           data-toggle="tooltip" data-placement="top" title="Edit"
                                           class="btn btn-warning btn-xs btn-flat"><i class="fa  fa-edit"></i></a>
                                    </div>
                                @endif
                                @if(Auth::user()->role == 0)
                                    <div class="col col-sm-2">
                                        <form class="" action="{{ route('staf.delete', $u->id) }}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button onclick="window.alert('Yakin akan menghapus data ?')"
                                                    type="submit"
                                                    data-toggle="tooltip" data-placement="top" title="Hapus"
                                                    name="button" class="btn btn-danger btn-xs btn-flat"><i
                                                        class="fa fa-trash-o"></i></button>
                                        </form>
                                    </div>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection