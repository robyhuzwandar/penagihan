@extends('admin')
@section('content')
    <!-- Main content -->
    <div class="box box-warning">
        <div class="box-header">
            <h3 class="box-title">Edit Data Staf {{ $user->nama }}</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col col-md-5">
                <form method="POST" action="{{ route('staf.update', $user->id) }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="form-group has-feedback{{ $errors->has('nama') ? ' has-error' : '' }}">
                        <input placeholder="Fullname" id="nama" type="text" class="form-control" name="nama"
                               value="{{ $user->nama }}" required autofocus>
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if ($errors->has('name'))
                            <span class="help-block"><strong>{{ $errors->first('nama') }}</strong></span>
                        @endif
                    </div>

                    <div class="form-group has-feedback{{ $errors->has('alamat') ? ' has-error' : '' }}">
                        <input placeholder="Alamat" id="alamat" type="text" class="form-control" name="alamat"
                               value="{{ $user->alamat }}" required autofocus>
                        <span class="fa fa-home form-control-feedback"></span>
                        @if ($errors->has('alamat'))
                            <span class="help-block"><strong>{{ $errors->first('alamat') }}</strong></span>
                        @endif
                    </div>

                    <div class="form-group has-feedback{{ $errors->has('nohp') ? ' has-error' : '' }}">
                        <input placeholder="No hp" id="nohp" type="text" class="form-control" name="nohp"
                               value="{{ $user->nohp }}" required autofocus>
                        <span class="fa  fa-phone form-control-feedback"></span>
                        @if ($errors->has('nohp'))
                            <span class="help-block"><strong>{{ $errors->first('nohp') }}</strong></span>
                        @endif
                    </div>

                    <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input placeholder="Email" id="email" type="email" class="form-control" name="email"
                               value="{{ $user->email }}" required autofocus>
                        <span class="fa  fa-phone form-control-feedback"></span>
                        @if ($errors->has('email'))
                            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                        @endif
                    </div>

                    <div class="form-group has-feedback{{ $errors->has('foto') ? ' has-error' : '' }}">
                        <img style="padding: 5px" width="90px"
                             src="{{ asset('fotoStaf/'.$user->foto) }}">
                        <input id="foto" type="file" class="form-control" name="foto" value="{{ old('foto') }}">
                        @if ($errors->has('foto'))
                            <span class="help-block"><strong>{{ $errors->first('foto') }}</strong></span>
                        @endif
                    </div>

                    <div class="form-group has-feedback">
                        <select name="role" class="form-control">
                            <option onselect="false">Pilih Role</option>

                            <option
                                    @if($user->role == 0)
                                    selected
                                    @endif
                                    value="0">Admin
                            </option>
                            <option
                                    @if($user->role == 1)
                                    selected
                                    @endif
                                    value="1">Staf
                            </option>
                            @if ($errors->has('role'))
                                <span class="help-block"><strong>{{ $errors->first('role') }}</strong></span>
                            @endif
                        </select>
                    </div>

                    <div class="form-group has-feedback{{ $errors->has('username') ? ' has-error' : '' }}">
                        <input placeholder="Username" id="username" type="text" class="form-control" name="username"
                               value="{{ $user->username }}" required>
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if ($errors->has('username'))
                            <span class="help-block"><strong>{{ $errors->first('username') }}</strong></span>
                        @endif
                    </div>

                    <span class="label label-danger">Kosongkan Kolom Password Jika tidak ingin merubah password</span>
                    <br>
                    <br>
                    <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input  id="password" placeholder="Password Baru" type="password" class="form-control"
                               name="password"
                               >
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        @if ($errors->has('password'))
                            <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                        @endif
                    </div>

                    <div class="form-group has-feedback">
                        <input placeholder="Ulangi password" id="password-confirm" type="password"
                               class="form-control"
                               name="password_confirmation">
                        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                    </div>


                    <button type="submit" class="btn btn-primary btn-flat">
                        Simpan
                    </button>

                </form>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
@endsection