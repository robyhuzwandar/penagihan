@extends('admin')
@section('content')
    <style>
        table.dataTable.select tbody tr,
        table.dataTable thead th:first-child {
            cursor: pointer;
        }
    </style>
    <!-- Main content -->
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title">Pembagian Tugas Penagihan</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @if(Auth::user()->role == 0)
                <form action="{{ route('pembagian.store') }}" method="POST">
                    <table id="example" class="display select table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        {{ csrf_field() }}
                        <tr>
                            <th>No.</th>
                            @if(Auth::user()->role == 0)
                                <th>Pilih</th>
                            @endif
                            <th>Staf Penagih</th>
                            <th>Nama Pelanggan</th>
                            <th>Alamat</th>
                            <th>No Hp</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pelanggan as $p)
                            <tr>
                                <td width="3%">{{ $loop->index + 1 }}</td>
                                @if($p->statusPetugasPenagihan == 0)
                                    <td width="2%"><input type="checkbox" name="id_pelanggan[]" value="{{ $p->id }}">
                                    </td>
                                @else
                                    <td width="2%">
                                        <a href="/pembagian/index/{{$p->id}}/batal">
                                            <small data-toggle="tooltip" data-placement="top"
                                                   title="Batalkan Staf Penagih" class="label label-success">
                                                <i class="fa fa-times"></i> Batal
                                            </small>
                                        </a>
                                @endif
                                <td id="#" width="16%">{{ $p->stafNama }}</td>
                                <td id="#" width="16%">{{ $p->nama }}</td>
                                <td width="16%">{{ $p->alamat }}</td>
                                <td width="16%">{{ $p->nohp }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="col col-md-4">
                        <div class="form-group {{ $errors->has('staf') ? ' has-error' : '' }}">
                            <label>Pilih Staf</label><br>
                            <select id="basic2" class="show-tick form-control" name="staf" multiple>
                                <option disabled="" selected="">Pilih Staf</option>
                                @foreach($user as $s)
                                    <option value="{{ $s->id }}">{{ $s->nama }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('staf'))
                                <span class="help-block"><strong>{{ $errors->first('staf') }}</strong></span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat" name="submit">
                            <i class="fa fa-check"> Simpan</i></button>
                    </div>
                </form>
            @else
                <h1 class="text-center">Anda Tidak Memiliki Akses</h1>
            @endif
        </div>
        <!-- /.box-body -->
    </div>
@endsection












