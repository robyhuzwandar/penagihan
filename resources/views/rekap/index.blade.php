@extends('admin')
@section('content')
    <!-- Main content -->
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title">Rekapan Pembayaran Details</h3>
        </div>
        <div class="box-body">
            <table id="example" class="table table-striped table-bordered" border="1" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Pelanggan</th>
                    <th>Alamat</th>
                    <th>No Hp</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($pelanggan as $p)
                    <tr>
                        <td width="3%">{{ $loop->index + 1 }}</td>
                        <td width="16%">{{ $p->nama }}</td>
                        <td width="16%">{{ $p->alamat }}</td>
                        <td width="16%">{{ $p->nohp }}</td>
                        <td width="16%">
                            <button type="button" class="btn btn-info btn-xs"
                                    data-toggle="modal" data-target-id="{{ $p->kodeSewa }}"
                                    data-target="#myModal">
                                <i class="fa fa-eye"></i> Details
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Details Pembayaran</h4>
                        </div>
                        <div class="modal-body">
                            <div class="fect-modal-body"></div>
                        </div>
                    </div>
                </div>
            </div>

            {{--Total Pendapatan--}}


        </div>
    </div>
@endsection