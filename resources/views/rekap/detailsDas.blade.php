<div class="table-responsive">
    <table id="example" class="table table-striped table-bordered" border="1" cellspacing="0"
           width="100%">
        <thead>
        <tr>
            <th>No</th>
            <th>Pelanggan</th>
            <th>Jumlah Bayar</th>
            <th>Tanggal Bayar</th>
        </tr>

        </thead>
        <tbody>
        @foreach($detailsDas as $d)
            <tr>
                <td width="3%">{{ $loop->index + 1 }}</td>
                <td width="16%">{{ $d->namaPelanggan }}</td>
                <td width="16%">{{ number_format($d->jb) }}</td>
                <td width="16%">{{ $d->tglBayar }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>