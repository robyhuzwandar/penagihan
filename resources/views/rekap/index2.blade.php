@extends('admin')
@section('content')
    <!-- Main content -->
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title">Rekapan Laporan Pembayaran</h3>
        </div>
        <div class="box-body">
            <table id="example" class="table table-striped table-bordered" border="1" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th rowspan="2" class="text-center">No</th>
                    <th rowspan="2" class="text-center">Nama</th>
                    <th rowspan="2" class="text-center">Bulan Mulai</th>
                    <th colspan="12" class="text-center">Bulan</th>
                </tr>
                <tr>
                    @foreach($bulanAll as $bln)
                        <th>{{ $bln->nama }}</th>
                    @endforeach
                    {{--<th>Action</th>--}}
                </tr>
                </thead>
                <tbody>
                @foreach($pelanggan as $p)
                    @if($p->pelangganNama != NULL)
                        <tr>
                            <td width="3%">{{ $loop->index}}</td>
                            <td width="16%"> {{ $p->pelangganNama }}</td>
                            <td width="16%">
                                <small data-toggle="tooltip" data-placement="top"
                                       title="{{ $p->stafNama }}" class="label label-success">
                                    @if($p->bulanMulai == 1)
                                        Januari
                                    @elseif($p->bulanMulai == 2)
                                        Februari
                                    @elseif($p->bulanMulai == 3)
                                        Maret
                                    @elseif($p->bulanMulai == 4)
                                        April
                                    @elseif($p->bulanMulai == 5)
                                        Mei
                                    @elseif($p->bulanMulai == 6)
                                        Juni
                                    @elseif($p->bulanMulai == 7)
                                        Juli
                                    @elseif($p->bulanMulai == 8)
                                        Agustus
                                    @elseif($p->bulanMulai == 9)
                                        September
                                    @elseif($p->bulanMulai == 10)
                                        Oktober
                                    @elseif($p->bulanMulai == 11)
                                        November
                                    @elseif($p->bulanMulai == 12)
                                        Desember
                                    @endif
                                </small>


                            </td>
                            <td width="16%">{{ number_format( $p->Januari) }}</td>
                            <td width="16%">{{ number_format($p->Februari) }}</td>
                            <td width="16%">{{ number_format($p->Maret) }}</td>
                            <td width="16%">{{ number_format($p->April) }}</td>
                            <td width="16%">{{ number_format($p->Mei) }}</td>
                            <td width="16%">{{ number_format($p->Juni) }}</td>
                            <td width="16%">{{ number_format($p->Juli) }}</td>
                            <td width="16%">{{ number_format($p->Agustus) }}</td>
                            <td width="16%">{{ number_format($p->September) }}</td>
                            <td width="16%">{{ number_format($p->Oktober) }}</td>
                            <td width="16%">{{ number_format($p->November) }}</td>
                            <td width="16%">{{ number_format($p->Desember) }}</td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection