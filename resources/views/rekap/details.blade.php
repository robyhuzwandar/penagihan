<div class="table-responsive">
    <table id="example" class="table table-striped table-bordered" border="1" cellspacing="0"
           width="100%">
        <thead>
        <tr>
            <th>No</th>
            <th>Nama Bulan</th>
            <th>Jumlah Bayar</th>
            <th>Tanggal Bayar</th>
            <th>Staf Penagih</th>
            <th>Action</th>
        </tr>

        </thead>
        <tbody>
        @foreach($details as $d)
            <tr>
                <td width="3%">{{ $loop->index + 1 }}</td>
                <td width="16%">{{ $d->nama }}</td>
                <td width="16%">{{ number_format($d->jb) }}</td>
                <td width="16%">{{ $d->tglBayar }}</td>
                <td width="16%">{{ $d->namaStaf }}</td>
                <td width="16%" class="text-center">
                    <form class="" action="{{ route('details.destroy', $d->kodeBayar)}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button onclick="window.alert('Hapus pelanggan ?')" type="submit"
                                data-toggle="tooltip" data-placement="top" title="Hapus"
                                name="button" class="btn btn-md btn-danger btn-flat"><span
                                    class="glyphicon glyphicon-trash"></span> Hapus
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>