@extends('admin')
@section('content')
    <!-- Main content -->
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title">Rekap Pendapatan</h3>
        </div>
        <div class="box-body">
            <table id="example" class="table table-striped table-bordered" border="1" cellspacing="0"
                   width="100%">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Bulan</th>
                    <th>Total Pendapatan</th>
                    <th>Dari</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pendapatan as $p)
                    <tr>
                        <td>{{ $loop->index +1 }}</td>
                        <td>{{  $p->nama }}</td>
                        <td>{{ number_format($p->jumlahBayar) }}</td>
                        <td>{{ $p->bulan_id_count }} pelanggan</td>
                        <td>
                            <button type="button" class="btn btn-info btn-xs"
                                    data-toggle="modal" data-target-id="{{ $p->id }}"
                                    data-target="#myModaldas">
                                <i class="fa fa-eye"></i> Details
                            </button>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="modal fade" id="myModaldas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Details Pembayaran</h4>
                        </div>
                        <div class="modal-body">
                            <div class="details-modal-body"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection