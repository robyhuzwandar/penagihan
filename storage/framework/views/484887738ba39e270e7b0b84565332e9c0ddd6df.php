<?php if(session('success')): ?>
    <div class="alert callout callout-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Sukses!</h4>
        <?php echo e(session('success')); ?>

    </div>
<?php endif; ?>

<?php if(session('danger')): ?>
    <div class="alert callout callout-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Gagal!</h4>
        <?php echo e(session('danger')); ?>

    </div>
<?php endif; ?>

<?php if(session('warning')): ?>
    <div class="alert callout callout-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Peringatan!</h4>
        <?php echo e(session('warning')); ?>

    </div>
<?php endif; ?>


