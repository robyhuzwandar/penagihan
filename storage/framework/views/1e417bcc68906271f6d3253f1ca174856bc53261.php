<?php $__env->startSection('content'); ?>
    <!-- Main content -->
    <div class="box box-warning">
        <div class="box-header">
            <h3 class="box-title">Edit Pelanggan</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <?php echo $__env->make('template.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <form action="<?php echo e(route('pelanggan.update', $pelanggan)); ?>" method="post"
                          enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        <?php echo e(method_field('PATCH')); ?>

                        <div class="form-group <?php echo e($errors->has('nama') ? ' has-error' : ''); ?>">
                            <label>Nama</label>
                            <input value="<?php echo e($pelanggan->nama); ?>" type="text" class="form-control" name="nama">
                            <?php if($errors->has('nama')): ?>
                                <span class="help-block"><strong><?php echo e($errors->first('nim')); ?></strong></span>
                            <?php endif; ?>
                        </div>

                        <div class="form-group <?php echo e($errors->has('alamat') ? ' has-error' : ''); ?>">
                            <label>Alamat</label>
                            <input value="<?php echo e($pelanggan->alamat); ?>" type="text" class="form-control" name="alamat">
                            <?php if($errors->has('alamat')): ?>
                                <span class="help-block"><strong><?php echo e($errors->first('alamat')); ?></strong></span>
                            <?php endif; ?>
                        </div>

                        <div class="form-group <?php echo e($errors->has('nohp') ? ' has-error' : ''); ?>">
                            <label>No HP</label>
                            <input value="<?php echo e($pelanggan->nohp); ?>" type="text" class="form-control" name="nohp">
                            <?php if($errors->has('nohp')): ?>
                                <span class="help-block"><strong><?php echo e($errors->first('nohp')); ?></strong></span>
                            <?php endif; ?>
                        </div>

                        <div class="form-group  <?php echo e($errors->has('bandwith') ? ' has-error' : ''); ?>">
                            <label>Bandwith</label>
                            <input value="<?php echo e($pelanggan->bandwith); ?>" type="text" class="form-control" name="bandwith">
                            <?php if($errors->has('bandwith')): ?>
                                <span class="help-block"><strong><?php echo e($errors->first('bandwith')); ?></strong></span>
                            <?php endif; ?>
                        </div>

                        <div class="form-group  <?php echo e($errors->has('jumlahBayar') ? ' has-error' : ''); ?>">
                            <label>Jumlah Bayar</label>
                            <input value="<?php echo e($pelanggan->jumlahBayar); ?>" type="text" class="form-control"
                                   name="jumlahBayar">
                            <?php if($errors->has('jumlahBayar')): ?>
                                <span class="help-block"><strong><?php echo e($errors->first('jumlahBayar')); ?></strong></span>
                            <?php endif; ?>
                        </div>

                        <div class="form-group">
                            <label>Pilih Bulan Mulai</label><br>
                            <select id="basic3" class="show-tick form-control" name="bulan" multiple>
                                <option disabled="">Pilih Bulan</option>
                                <?php $__currentLoopData = $bulan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $b): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option
                                            <?php if($b->id == $pelanggan->bulan_id): ?>
                                            selected
                                            <?php endif; ?>
                                            value="<?php echo e($b->id); ?>"><?php echo e($b->nama); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>

                        <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>