<?php $__env->startSection('content'); ?>
    <!-- Main content -->
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title">Data Pelanggan</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>No Hp</th>
                    <th>Mulai Berlangganan</th>
                    <th>Bandwith</th>
                    <th>Jumlah Bayar</th>
                    <?php if(Auth::user()->role == 0): ?>
                        <th>Action</th>
                    <?php endif; ?>
                </tr>
                </thead>
                <tbody>

                <?php $__currentLoopData = $pelanggan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td width="3%"><?php echo e($loop->index + 1); ?></td>
                        <td id="#" width="16%"><?php echo e($p->nama); ?></td>
                        <td width="16%"><?php echo e($p->alamat); ?></td>
                        <td width="16%"><?php echo e($p->nohp); ?></td>
                        <td width="16%"><?php echo e($p->namaBulan); ?></td>
                        <td width="16%"><?php echo e($p->bandwith); ?></td>
                        <td width="16%"><?php echo e(number_format($p->jumlahBayar)); ?></td>
                        <td width="16%" class="text-center">
                            <?php if(Auth::user()->role == 0): ?>
                                <div class="row">
                                    <div class="col col-sm-2">
                                        <a href="<?php echo e(route('pelanggan.edit', $p->id)); ?>"
                                           data-toggle="tooltip" data-placement="top" title="Edit"
                                           class="btn btn-xs btn-warning btn-flat"><i
                                                    class="glyphicon glyphicon-edit"></i></a>
                                    </div>
                                    <div class="col col-sm-2">
                                        <form class="" action="<?php echo e(route('pelanggan.destroy', $p->id)); ?>" method="post">
                                            <?php echo e(csrf_field()); ?>

                                            <?php echo e(method_field('DELETE')); ?>

                                            <button onclick="window.alert('Hapus pelanggan ?')" type="submit"
                                                    data-toggle="tooltip" data-placement="top" title="Hapus"
                                                    name="button" class="btn btn-xs btn-danger btn-flat"><span
                                                        class="glyphicon glyphicon-trash"></span></button>
                                        </form>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>

            </table>
        </div>
        <!-- /.box-body -->
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>