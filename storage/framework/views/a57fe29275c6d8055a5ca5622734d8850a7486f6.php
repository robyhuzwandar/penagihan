<?php $__env->startSection('content'); ?>
    <!-- Main content -->
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title">Tambahkan Pelanggan</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <?php echo $__env->make('template.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <form action="<?php echo e(route('pelanggan.store')); ?>" method="post" enctype="multipart/form-data"
                          id="contactForm">
                        <?php echo e(csrf_field()); ?>

                        <div class="form-group <?php echo e($errors->has('nama') ? ' has-error' : ''); ?>">
                            <label>Nama</label>
                            <input value="<?php echo e(old('nama')); ?>" type="text" class="form-control" name="nama">
                            <?php if($errors->has('nama')): ?>
                                <span class="help-block"><strong><?php echo e($errors->first('nim')); ?></strong></span>
                            <?php endif; ?>
                        </div>

                        <div class="form-group <?php echo e($errors->has('alamat') ? ' has-error' : ''); ?>">
                            <label>Alamat</label>
                            <input value="<?php echo e(old('alamat')); ?>" type="text" class="form-control" name="alamat">
                            <?php if($errors->has('alamat')): ?>
                                <span class="help-block"><strong><?php echo e($errors->first('alamat')); ?></strong></span>
                            <?php endif; ?>
                        </div>

                        <div class="form-group <?php echo e($errors->has('nohp') ? ' has-error' : ''); ?>">
                            <label>No HP</label>
                            <input value="<?php echo e(old('nohp')); ?>" type="number" class="form-control" name="nohp">
                            <?php if($errors->has('nohp')): ?>
                                <span class="help-block"><strong><?php echo e($errors->first('nohp')); ?></strong></span>
                            <?php endif; ?>
                        </div>

                        <div class="form-group  <?php echo e($errors->has('bandwith') ? ' has-error' : ''); ?>">
                            <label>Bandwith</label>
                            <input value="<?php echo e(old('bandwith')); ?>" type="text" class="form-control" name="bandwith">
                            <?php if($errors->has('bandwith')): ?>
                                <span class="help-block"><strong><?php echo e($errors->first('bandwith')); ?></strong></span>
                            <?php endif; ?>
                        </div>

                        <div class="form-group  <?php echo e($errors->has('jumlahBayar') ? ' has-error' : ''); ?>">
                            <label>Jumlah Bayar</label>
                            <input value="<?php echo e(old('jumlahBayar')); ?>" type="number" class="form-control" name="jumlahBayar">
                            <?php if($errors->has('jumlahBayar')): ?>
                                <span class="help-block"><strong><?php echo e($errors->first('jumlahBayar')); ?></strong></span>
                            <?php endif; ?>
                        </div>


                        <div class="form-group <?php echo e($errors->has('bulan') ? ' has-error' : ''); ?>">
                            <label>Pilih Bulan Mulai</label><br>
                            <select id="basic3" class="show-tick form-control" name="bulan" multiple>
                                <option disabled="" selected="">Pilih Bulan</option>
                                <?php $__currentLoopData = $bulan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $b): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($b->id); ?>"><?php echo e($b->nama); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                            <?php if($errors->has('bulan')): ?>
                                <span class="help-block"><strong><?php echo e($errors->first('bulan')); ?></strong></span>
                            <?php endif; ?>
                        </div>

                        <button type="submit" class="btn btn-primary btn-flat" name="submit"><span
                                    class="glyphicon glyphicon-send"></span> Simpan
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>