<?php $__env->startSection('content'); ?>
    <style>
        table.dataTable.select tbody tr,
        table.dataTable thead th:first-child {
            cursor: pointer;
        }
    </style>
    <!-- Main content -->
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title">Pembagian Tugas Penagihan</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?php if(Auth::user()->role == 0): ?>
                <form action="<?php echo e(route('pembagian.store')); ?>" method="POST">
                    <table id="example" class="display select table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <?php echo e(csrf_field()); ?>

                        <tr>
                            <th>No.</th>
                            <?php if(Auth::user()->role == 0): ?>
                                <th>Pilih</th>
                            <?php endif; ?>
                            <th>Staf Penagih</th>
                            <th>Nama Pelanggan</th>
                            <th>Alamat</th>
                            <th>No Hp</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $pelanggan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td width="3%"><?php echo e($loop->index + 1); ?></td>
                                <?php if($p->statusPetugasPenagihan == 0): ?>
                                    <td width="2%"><input type="checkbox" name="id_pelanggan[]" value="<?php echo e($p->id); ?>">
                                    </td>
                                <?php else: ?>
                                    <td width="2%">
                                        <a href="/pembagian/index/<?php echo e($p->id); ?>/batal">
                                            <small data-toggle="tooltip" data-placement="top"
                                                   title="Batalkan Staf Penagih" class="label label-success">
                                                <i class="fa fa-times"></i> Batal
                                            </small>
                                        </a>
                                <?php endif; ?>
                                <td id="#" width="16%"><?php echo e($p->stafNama); ?></td>
                                <td id="#" width="16%"><?php echo e($p->nama); ?></td>
                                <td width="16%"><?php echo e($p->alamat); ?></td>
                                <td width="16%"><?php echo e($p->nohp); ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>

                    <div class="col col-md-4">
                        <div class="form-group <?php echo e($errors->has('staf') ? ' has-error' : ''); ?>">
                            <label>Pilih Staf</label><br>
                            <select id="basic2" class="show-tick form-control" name="staf" multiple>
                                <option disabled="" selected="">Pilih Staf</option>
                                <?php $__currentLoopData = $user; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($s->id); ?>"><?php echo e($s->nama); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                            <?php if($errors->has('staf')): ?>
                                <span class="help-block"><strong><?php echo e($errors->first('staf')); ?></strong></span>
                            <?php endif; ?>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat" name="submit">
                            <i class="fa fa-check"> Simpan</i></button>
                    </div>
                </form>
            <?php else: ?>
                <h1 class="text-center">Anda Tidak Memiliki Akses</h1>
            <?php endif; ?>
        </div>
        <!-- /.box-body -->
    </div>
<?php $__env->stopSection(); ?>













<?php echo $__env->make('admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>