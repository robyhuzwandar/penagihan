<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel" style="height: 60px">
            <div class="pull-left image">
                <img src="<?php echo e(asset('fotoStaf/'. Auth::user()->foto)); ?>" style=" height:45px; width:auto important;"
                     class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo e(Auth::user()->nama); ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="<?php echo e(route('pelanggan.pencarian')); ?>" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search by name">
                <span class="input-group-btn">
                    <a href="<?php echo e(route('pelanggan.pencarian')); ?>">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                <i class="fa fa-search"></i>
              </button>
                        </a>
            </span>
            </div>
        </form>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="">
                <a href="/">
                    <i class="treeview glyphicon glyphicon-home"></i> <span>Dashboard</span>
                </a>
            </li>

            <li class="treeview <?php echo e(set_active(['pelanggan.create', 'pelanggan.index'])); ?>">
                <a href="">
                    <i class="fa fa-users"></i> <span>Pelanggan</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo e(set_active('pelanggan.index')); ?>"><a href="<?php echo e(route('pelanggan.index')); ?>"><i
                                    class="fa fa-circle-o"></i>List Pelanggan</a></li>
                    <li class="<?php echo e(set_active('pelanggan.create')); ?>"><a href="<?php echo e(route('pelanggan.create')); ?>"><i
                                    class="fa fa-circle-o"></i>Tambah Pelanggan</a></li>
                </ul>
            </li>
            <?php if(Auth::user()->role == 0): ?>

                <li class="<?php echo e(set_active(['pembagian.index'])); ?>">
                    <a href="<?php echo e(route('pembagian.index')); ?>">
                        <i class="fa fa-spinner"></i> <span>Pembagian Tugas</span>
                    </a>
                </li>
            <?php endif; ?>
            <li class="<?php echo e(set_active(['pembayaran.index'])); ?>">
                <a href="<?php echo e(route('pembayaran.index')); ?>">
                    <i class="fa fa-tag"></i> <span>Pembayaran</span>
                </a>
            </li>
            <li class="treeview <?php echo e(set_active(['rekap.index2', 'rekap.index','rekap.pendapatan'])); ?>">
                <a href="">
                    <i class="fa fa-file"></i> <span>Rekapan</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo e(set_active('rekap.index')); ?>"><a href="<?php echo e(route('rekap.index')); ?>">
                            <i class="fa fa-circle-o"></i>Rekap Pembayaran Details</a></li>
                    <li class="<?php echo e(set_active('rekap.index2')); ?>"><a href="<?php echo e(route('rekap.index2')); ?>">
                            <i class="fa fa-circle-o"></i>Rekap Pembayaran</a></li>
                    <li class="<?php echo e(set_active('rekap.pendapatan')); ?>"><a href="<?php echo e(route('rekap.pendapatan')); ?>">
                            <i class="fa fa-circle-o"></i>Rekap Pendapatan</a></li>
                </ul>
            </li>
            <li class="treeview <?php echo e(set_active(['staf.index', 'staf.create'])); ?>">
                <a href="">
                    <i class="fa fa-user"></i> <span>Staf</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo e(set_active('staf.index')); ?>"><a href="<?php echo e(route('staf.index')); ?>"><i
                                    class="fa fa-circle-o"></i>List Staf</a></li>
                    <?php if(Auth::user()->role == 0): ?>
                        <li class="<?php echo e(set_active('staf.create')); ?>"><a href="<?php echo e(route('staf.create')); ?>"><i
                                        class="fa fa-circle-o"></i>Tambah Staf</a></li>
                    <?php endif; ?>
                </ul>
            </li>
        </ul>
    </section>
</aside>