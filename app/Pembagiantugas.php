<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembagiantugas extends Model
{
    protected $table = 'pembagianTugas';
    public $timestamps = false;
    protected $fillable = ['staf_id', 'pelanggan_id'];

    public function staf()
    {
        return $this->belongsTo('App\User');
    }

    public function pelanggan()
    {
        return $this->belongsTo('App\Pelanggan');
    }


}
