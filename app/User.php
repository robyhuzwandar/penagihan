<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    public $table = 'staf';

    protected $fillable = [
        'nama', 'username', 'pass', 'foto', 'role', 'alamat', 'nohp', 'password', 'email'
    ];

    public function pembagianTugas()
    {
        return $this->hasMany('App\Pembagiantugas');
    }

    protected $hidden = [
        'password', 'remember_token',
    ];
}
