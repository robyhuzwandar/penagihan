<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bulan extends Model
{
    protected $table = 'bulan';
    public $timestamps = false;
    protected $fillable = ['nama'];

    public function details()
    {
        return $this->hasMany('App\Details');
    }
}
