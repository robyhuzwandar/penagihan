<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelanggan extends Model
{
    protected $table = "pelanggan";
//    public $timestamps = false;
    protected $fillable = ['nama', 'alamat', 'nohp', 'bandwith', 'bulan_id', 'statusPetugasPenagihan', 'kodeSewa','jumlahBayar'];


    public function pembagianTugas()
    {
        return $this->hasMany('App\Pembagiantugas');
    }

    public function pembayaran()
    {
        return $this->hasMany('App\Pembayaran');
    }

    public function bulan()
    {
        return $this->belongsTo('App\Bulan');
    }
}
