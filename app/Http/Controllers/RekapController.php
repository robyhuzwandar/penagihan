<?php

namespace App\Http\Controllers;

use App\Bulan;
use App\Details;
use App\Pelanggan;
use App\Pembayaran;
use App\View_rekap;
use Illuminate\Support\Facades\DB;

class RekapController extends Controller
{

    public function viewDetails($kodeSewa)
    {
        $details = DB::table('bulan')
            ->rightJoin('details', 'bulan.id', '=', 'details.bulan_id')
            ->rightJoin('pembayaran', 'pembayaran.kodeBayar', '=', 'details.kodeBayar')
            ->rightJoin('staf', 'staf.id', '=', 'pembayaran.staf_id')
            ->select('details.jumlahBayar as jb', 'details.kodeBayar as kodeBayar', 'bulan.*', 'details.tglBayar as tglBayar',
                'staf.nama as namaStaf')
            ->where('kodeSewa', '=', $kodeSewa)->get();
        return view('rekap.details', compact('details'));
    }

    public function index()
    {
        $bulanAll = Bulan::all();
        $pelanggan = DB::table('pelanggan')
            ->leftJoin('pembayaran', 'pelanggan.id', '=', 'pembayaran.pelanggan_id')
            ->leftJoin('details', 'pembayaran.kodeBayar', '=', 'details.kodeBayar')
            ->leftJoin('bulan', 'bulan.id', '=', 'details.bulan_id')
            ->select(
                DB::raw('count(details.kodeSewa) as kodeSewa_count'),
                'bulan.nama as namaBulan', 'details.jumlahBayar as jumlahBayar',
                'details.bulan_id as Pbulan_id', 'pembayaran.pelanggan_id as Ppelanggan_id',
                'details.kodeSewa as kodeSewa',
                'pelanggan.*')
            ->groupBy('pelanggan.nama')
            ->orderBy('pelanggan.id')
            ->get();
        return view('rekap.index', compact('bulanAll', 'pelanggan'));
    }

    public function index2()
    {
        $bulanAll = Bulan::all();
        $pelanggan = View_rekap::all();
        $pembayaran = Pembayaran::all();
        return view('rekap.index2', compact('bulanAll', 'details', 'pelanggan', 'pembayaran'));
    }

    public function jumlahPendapatanTiapBulan()
    {
        $pendapatan = DB::table('bulan')
            ->leftJoin('details', 'details.bulan_id', '=', 'bulan.id')
            ->select(
                DB::raw('SUM(details.jumlahBayar) as jumlahBayar'),
                DB::raw('count(DISTINCT details.kodeSewa) as bulan_id_count'),
                'bulan.*', 'details.kodeSewa as kodeSewa', 'details.bulan_id as Dbulan_id',
                'details.bulan_id as bulan_id')
            ->groupBy('bulan.id')
            ->orderBy('bulan.id')
            ->get();
        return view('rekap.pendapatan', compact('pendapatan'));
    }

    public function viewDetailsDas($bulan_id)
    {
        $detailsDas = DB::table('bulan')
            ->rightJoin('details', 'bulan.id', '=', 'details.bulan_id')
            ->rightJoin('pembayaran', 'pembayaran.kodeBayar', '=', 'details.kodeBayar')
            ->rightJoin('pelanggan', 'pelanggan.id', '=', 'pembayaran.pelanggan_id')
            ->select('details.jumlahBayar as jb', 'pelanggan.nama as namaPelanggan',
                'details.tglBayar as tglBayar')
            ->where('bulan.id', '=', $bulan_id)
            ->get();
        return view('rekap.detailsDas', compact('detailsDas'));
    }
}
