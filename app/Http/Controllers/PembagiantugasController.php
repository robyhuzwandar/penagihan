<?php

namespace App\Http\Controllers;

use App\Pelanggan;
use App\Pembagiantugas;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PembagiantugasController extends Controller
{
    public function index()
    {
        $user = User::all();
        $pelanggan = DB::table('pelanggan as p')
            ->leftJoin('pembagianTugas as pt', 'p.id', '=', 'pt.pelanggan_id')
            ->leftjoin('staf as s', 'pt.staf_id', '=', 's.id')
            ->select('p.*', 's.nama as stafNama')
            ->groupBy('p.nama')
            ->orderBy('p.id')
            ->get();
        return view('pembagian.index', compact('pembagianTugas', 'user', 'pelanggan'));
    }


    public function store(Request $request, Pelanggan $pelanggan)
    {
        $request->validate([
            'staf' => 'required',
            'id_pelanggan' => 'required'
        ]);

        $staf_id = $request->staf;
        $pelanggan_id = $request->id_pelanggan;

        foreach ($pelanggan_id as $key => $value) {

            Pembagiantugas::create([
                'staf_id' => $staf_id,
                'pelanggan_id' => $value
            ]);
        }

        $pelanggan->update(['statusPetugasPenagihan' => 1]);
        return redirect()->back();
    }

    public function destroy(Pembagiantugas $pembagiantugas)
    {
        $pembagiantugas->delete();
        return redirect()->back();
    }

    public function batal(Pelanggan $pelanggan)
    {
        $pelanggan->update(['statusPetugasPenagihan' => 0]);
        DB::table('pembagianTugas')->where('pelanggan_id', $pelanggan->id)->delete();
        return redirect()->back();
    }
}
