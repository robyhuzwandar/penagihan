<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        return view('staf.index', compact('user'));
    }

    public function profile()
    {
        $user = User::all();
        return view('staf.profile', compact('user'));
    }

    public function edit($user)
    {
        $user = User::find($user);

        return view('staf.edit', compact('user'));
    }

    public function create()
    {
        return view('staf.create');
    }

    protected function update($user)
    {
        $staf = User::find($user);
        $f = request()->file('foto');

        if (!empty($f)) {
            $foto = time() . '.' . $f->getClientOriginalExtension(); //namagambar
            request()->foto->move(public_path('fotoStaf'), $foto); //memindahkan gambar kefolder

            $stafF = User::where('id', $staf->id)->first();
            $file_path = public_path() . '/fotoStaf/' . $stafF->foto;
            unlink($file_path);

            if (!empty(request('password'))) {
                $staf->update([
                    'nama' => request('nama'),
                    'username' => request('username'),
                    'foto' => $foto,
                    'role' => request('role'),
                    'alamat' => request('alamat'),
                    'email' => request('email'),
                    'password' => bcrypt(request('password')),
                    'nohp' => request('nohp')
                ]);
            } else {
                $staf->update([
                    'nama' => request('nama'),
                    'username' => request('username'),
                    'foto' => $foto,
                    'role' => request('role'),
                    'alamat' => request('alamat'),
                    'email' => request('email'),
                    'nohp' => request('nohp')
                ]);
            }

        } else {
            if (!empty(request('password'))) {
                $staf->update([
                    'nama' => request('nama'),
                    'username' => request('username'),
                    'role' => request('role'),
                    'alamat' => request('alamat'),
                    'email' => request('email'),
                    'password' => bcrypt(request('password')),
                    'nohp' => request('nohp')
                ]);
            } else {
                $staf->update([
                    'nama' => request('nama'),
                    'username' => request('username'),
                    'role' => request('role'),
                    'alamat' => request('alamat'),
                    'email' => request('email'),
                    'nohp' => request('nohp')
                ]);
            }
        }

        return redirect()->route('staf.index');
    }

    protected function store(User $user)
    {

        $f = request('foto');
        if (!empty($f)) {
            $foto = time() . '.' . $f->getClientOriginalExtension(); //namagambar
            request()->foto->move(public_path('fotoStaf'), $foto); //memindahkan gambar kefolder

            $user->create(
                [
                    'nama' => request('nama'),
                    'username' => request('username'),
                    'password' => bcrypt(request('password')),
                    'foto' => $foto,
                    'role' => 1,
                    'alamat' => request('alamat'),
                    'email' => request('email'),
                    'nohp' => request('nohp')
                ]
            );

        } else {
            $user->create(
                [
                    'nama' => request('nama'),
                    'username' => request('username'),
                    'password' => bcrypt(request('password')),
                    'role' => 1,
                    'alamat' => request('alamat'),
                    'email' => request('email'),
                    'nohp' => request('nohp')
                ]
            );
        }

        return redirect()->back()->withSuccess('Data Berhasil di Simpan');
    }

    public function destroy(User $user)
    {
        $file_path = public_path() . '/fotoStaf/' . $user->foto;
        $user->delete();
        unlink($file_path);
        return redirect()->back();
    }
}
