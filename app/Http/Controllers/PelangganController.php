<?php

namespace App\Http\Controllers;

use App\Bulan;
use App\Pelanggan;
use App\View_rekap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PelangganController extends Controller
{
    public function create()
    {
        $bulan = Bulan::all();
        return view('pelanggan.create', compact('bulan'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|string|max:255',
            'alamat' => 'required|string|max:255',
            'nohp' => 'required|string|max:255',
            'bandwith' => 'required|string|max:255',
            'jumlahBayar' => 'required',
            'bulan' => 'required',
        ]);

        $kodeSewa = str_random(15);
        Pelanggan::create([
            'nama' => request('nama'),
            'alamat' => request('alamat'),
            'nohp' => request('nohp'),
            'bulan_id' => request('bulan'),
            'jumlahBayar' => request('jumlahBayar'),
            'bandwith' => $request->bandwith,
            'kodeSewa' => $kodeSewa,
            'statusPetugasPenagihan' => 0
        ]);
        return redirect()->back()->withSuccess('Data Berhasil di Simpan');
    }

    public function index()
    {
        $pelanggan = DB::table('pelanggan as p')
            ->join('bulan as b', 'b.id', '=', 'p.bulan_id')
            ->select('p.*', 'b.nama as namaBulan')
            ->get();
        return view('pelanggan.index', compact('pelanggan'));
    }

    public function pencarian(Request $request)
    {
        $key = $request->q;
        $pelanggan = DB::table('pelanggan as p')
            ->join('bulan as b', 'b.id', '=', 'p.bulan_id')
            ->select('p.*', 'b.nama as namaBulan')
            ->where('p.nama', 'LIKE', "%$key%")
            ->get();

        $bulanAll = Bulan::all();
        $pelangganBayar = DB::table('pelanggan')
            ->leftJoin('pembayaran', 'pelanggan.id', '=', 'pembayaran.pelanggan_id')
            ->leftJoin('details', 'pembayaran.kodeBayar', '=', 'details.kodeBayar')
            ->leftJoin('bulan', 'bulan.id', '=', 'details.bulan_id')
            ->select(
                DB::raw('count(details.kodeSewa) as kodeSewa_count'),
                'bulan.nama as namaBulan', 'details.jumlahBayar as jumlahBayar',
                'details.bulan_id as Pbulan_id', 'pembayaran.pelanggan_id as Ppelanggan_id',
                'details.kodeSewa as kodeSewa',
                'pelanggan.*')
            ->groupBy('pelanggan.nama')
            ->orderBy('pelanggan.id')
            ->where('pelanggan.nama', 'LIKE', "%$key%")
            ->get();

        $pelangganLapor = View_rekap::where('pelangganNama', 'LIKE', "%$key%")->get();
//        $pembayaranLapor = Pembayaran::all();
        return view('pelanggan.pencarian', compact('pelanggan', 'pelangganBayar', 'bulanAll', 'pelangganLapor'));
    }

    public function edit($id)
    {
        $bulan = Bulan::all();
        $pelanggan = Pelanggan::find($id);
        return view('pelanggan.edit', compact('pelanggan', 'bulan'));
    }

    public function update(Request $request, $id)
    {
        $pelanggan = Pelanggan::find($id);
        $pelanggan->update([
            'nama' => request('nama'),
            'alamat' => request('alamat'),
            'nohp' => request('nohp'),
            'bulan_id' => request('bulan'),
            'jumlahBayar' => request('jumlahBayar'),
            'bandwith' => $request->bandwith,
            'statusPetugasPenagihan' => 0
        ]);
        return redirect(route('pelanggan.index'));
    }

    public function updateStatusPelanggan($id)
    {
        $pelanggan = Pelanggan::find($id);
        $pelanggan->update([
            'statusPetugasPenagihan' => request('statusPenugasan')
        ]);
        return redirect(route('pembagian.index'));
    }

    public function destroy(Pelanggan $pelanggan)
    {
        $pelanggan->delete();
        return redirect()->back();
    }

    public function destroyAll()
    {
        Pelanggan::truncate();
        return redirect()->back();
    }
}
