<?php

namespace App\Http\Controllers;

use App\Bulan;
use App\Details;
use App\Pelanggan;
use App\Pembagiantugas;
use App\Pembayaran;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PembayaranController extends Controller
{
    public function index()
    {
        $stafId = Auth::user()->id;
        $pelanggan = DB::table('pembagianTugas as pt')
            ->join('pelanggan as p', 'p.id', '=', 'pt.pelanggan_id')
            ->join('staf as s', 's.id', '=', 'pt.staf_id')
            ->select('pt.*', 'p.nama as namaPelanggan', 'p.id as idPelanggan')
            ->where('pt.staf_id', '=', $stafId)
            ->get();
        $bulan = Bulan::all();
        return view('pembayaran.index', compact('pelanggan', 'bulan'));
    }


    public function store(Pembayaran $pembayaran)
    {
        request()->validate([
            'pelanggan' => 'required',
            'bulan' => 'required'
        ]);

        $idp = request('pelanggan');
        $stafId = Auth::user()->id;
        $pelanggan = Pelanggan::where('id', '=', $idp)->first();
        $kodeSewaP = $pelanggan->kodeSewa; //kodeSewa Di table Pelanggan
        $jb = $pelanggan->jumlahBayar;
        $kodeBayar = str_random(15);
        $tglBayar = date('Y-m-d');

        //membuat validasi
        $details = Details::where('kodeSewa', '=', $kodeSewaP)->get();
        $yearBayarSekarang = Carbon::createFromFormat('Y-m-d', $tglBayar)->year;

        $cek = Details::where('kodeSewa', $kodeSewaP)
            ->where('bulan_id', request('bulan'))
            ->whereYear('tglBayar', $yearBayarSekarang)
            ->first();
        if ($cek) {
            \Session::flash('danger', 'Periksa Bulan Pembayaran');
        } else {
            $pembayaran->create([
                'kodeBayar' => $kodeBayar,
                'pelanggan_id' => $idp,
                'staf_id' => $stafId,
            ]);
            Details::create([
                'bulan_id' => request('bulan'),
                'jumlahBayar' => $jb,
                'kodeSewa' => $kodeSewaP,
                'kodeBayar' => $kodeBayar,
                'tglBayar' => $tglBayar
            ]);
            \Session::flash('success', 'Pembayaran Berhasil');
        }
        return redirect()->back();
    }


    public function destroy($kodeBayar)
    {
        DB::table('pembayaran')
            ->where('kodeBayar', '=', $kodeBayar)
            ->delete();
        return redirect()->back();
    }

}
