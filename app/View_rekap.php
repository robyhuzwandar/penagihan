<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class View_rekap extends Model
{
    protected $table = "view_rekapHasil";
    public $timestamps = false;
    protected $fillable = [
        'pelangganNama'
        , 'Januari'
        , 'Februari'
        , 'Maret'
        , 'April'
        , 'Mei'
        , 'Juni'
        , 'Juli'
        , 'Agustus'
        , 'September'
        , 'Oktober'
        , 'November'
    ];
}