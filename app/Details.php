<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Details extends Model
{
    protected $table = 'details';
    public $timestamps = false;
    protected $fillable = ['bulan_id', 'kodeBayar', 'jumlahBayar','kodeSewa','tglBayar'];


    public function bulan()
    {
        return $this->belongsTo('App\Bulan');
    }

    public function pembayaran()
    {
        return $this->belongsTo('App\Pembayaran');
    }
}
