<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $table = 'pembayaran';
    public $timestamps = false;
    protected $fillable = ['pelanggan_id', 'staf_id','kodeBayar'];


    public function pelanggan()
    {
        return $this->belongsTo('App\Pelanggan');
    }

    public function details()
    {
        return $this->hasMany('App\Details');
    }
}
